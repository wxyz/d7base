; Core version
; ------------
; Each makefile should begin by declaring the core version of Drupal that all
; projects should be compatible with.

core = 7.x

; API version
; ------------
; Every makefile needs to declare its Drush Make API version. This version of
; drush make uses API version `2`.

api = 2

; Core project
; ------------

projects[drupal][version] = 7.44


; Modules / Themes
; --------
; All modules and themes are organized into their own sections. All contributed
; modules are put into the contrib subdirectory. Each module that has a
; corresponding library is added as well.


; Administration
projects[admin_menu][subdir] = contrib
projects[adminimal_admin_menu][subdir] = contrib
projects[adminimal_theme][]
projects[module_filter][subdir] = contrib
projects[views_bulk_operations][subdir] = contrib



; Design / Theming
projects[bootstrap][]
;projects[magic][subdir] = contrib
;projects[fences][subdir] = contrib
;projects[field_formatter_settings][subdir] = contrib
;projects[menu_attributes][subdir] = contrib

projects[jquery_update][subdir] = "contrib"
projects[jquery_update][version] = 3.0-alpha3

libraries[owl-carousel][type] = "libraries"
libraries[owl-carousel][download][type] = "file"
libraries[owl-carousel][download][url] = "https://github.com/OwlFonk/OwlCarousel/archive/master.zip"



; Development
;projects[browsersync][subdir] = contrib
;projects[delete_all][subdir] = contrib
projects[devel][subdir] = contrib
;projects[stage_file_proxy][subdir] = contrib

; Email
projects[mailsystem][subdir] = contrib
projects[mimemail][subdir] = contrib
;projects[newsletter][subdir] = contrib
projects[smtp][subdir] = contrib

; Features
;projects[features][subdir] = contrib
;projects[ctools][subdir] = contrib
;projects[strongarm][subdir] = contrib


; Fields
;projects[insert][subdir] = contrib
projects[entityreference][subdir] = contrib
;projects[diff][subdir] = contrib
;projects[addressfield][subdir] = contrib
;projects[countries][subdir] = contrib
projects[date][subdir] = contrib
projects[email][subdir] = contrib
projects[link][subdir] = contrib
;projects[field_collection][subdir] = contrib
projects[field_group][subdir] = contrib
;projects[url][subdir] = contrib
projects[title][subdir] = contrib

; Interantionalization
projects[i18n][subdir] = contrib
;projects[lang_dropdown][subdir] = contrib
projects[transliteration][subdir] = contrib


; Other
projects[entity][subdir] = contrib
projects[libraries][subdir] = contrib
projects[login_destination][subdir] = contrib
;projects[email_registration][subdir] = contrib
;projects[realname][subdir] = contrib
;projects[variable][subdir] = contrib
;projects[sshkey][subdir] = contrib
;projects[advanced_help][subdir] = contrib
projects[rules][subdir] = contrib
;projects[varnish][subdir] = contrib

; Maps
;projects[geofield][subdir] = contrib
;projects[geophp][subdir] = contrib
;projects[geocoder][subdir] = contrib
;projects[addressfield][subdir] = contrib
;projects[addressfield_autocomplete][subdir] = contrib
;projects[gmap][subdir] = contrib
;libraries[geocomplete][download][type] = get
;libraries[geocomplete][download][url] = ;https://github.com/ubilabs/geocomplete/archive/1.6.4.tar.gz
;libraries[geocomplete][directory_name] = geocomplete
;libraries[geocomplete][destination] = libraries



;Redis & libraries
;projects[redis][subdir] = contrib
;libraries[predis][download][type] = get
;libraries[predis][download][url] = https://github.com/nrk/predis/archive/v1.0.tar.gz
;libraries[predis][directory_name] = predis
;libraries[predis][destination] = libraries

; Panels
;projects[panels][subdir] = contrib
;projects[panelizer][subdir] = contrib


; SEO
projects[metatag][subdir] = contrib
projects[seo_checklist][subdir] = contrib
projects[checklistapi][subdir] = contrib
;projects[site_map][subdir] = contrib
projects[site_verify][subdir] = contrib
;projects[page_title][subdir] = contrib
projects[xmlsitemap][subdir] = contrib
projects[google_analytics][subdir] = contrib


; Site Building
;projects[auto_nodetitle][subdir] = contrib
;projects[block_class][subdir] = contrib
;projects[eck][subdir] = contrib
projects[draggableviews][subdir] = contrib
;projects[ds][subdir] = contrib
projects[pathauto][subdir] = contrib
;projects[subpathauto][subdir] = contrib
projects[multiupload_filefield_widget][subdir] = contrib
projects[multiupload_imagefield_widget][subdir] = contrib
;projects[nodequeue][subdir] = contrib
projects[token][subdir] = contrib
;projects[image_focus][subdir] = contrib
;projects[imagecache_token][subdir] = contrib
projects[webform][subdir] = contrib
;projects[webform_hints][subdir] = contrib



projects[media][subdir] = contrib
projects[media_youtube][subdir] = contrib


projects[wysiwyg][subdir] = "contrib"
projects[wysiwyg][version] = 2.2
projects[wysiwyg][patch][] = "http://drupal.org/files/wysiwyg-remove-breaks-1964806-2.patch"
projects[wysiwyg][patch][] = "http://drupal.org/files/wysiwyg-table-format.patch"

projects[wysiwyg_filter][subdir] = "contrib"
projects[wysiwyg_filter][version] = 1.6-rc2


libraries[ckeditor][download][type] = get
libraries[ckeditor][download][url] = http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.0.1/ckeditor_4.0.1_standard.tar.gz
libraries[ckeditor][destination] = libraries


; Social Media
;projects[oauth][subdir] = contrib
;projects[twitter][subdir] = contrib


; Views
projects[views][subdir] = contrib
;projects[better_exposed_filters][subdir] = contrib
;projects[views_argument_substitutions][subdir] = contrib
;projects[views_datasource][subdir] = contrib
projects[views_php][subdir] = contrib
;projects[views_send][subdir] = contrib
;projects[varnish][subdir] = contrib
